/**
 * Created by tata on 06.12.17.
 */


function Painter(ctx) {
    this.ctx = ctx;
    this.radius = 0;
    this.width = 0; // для прямоугольника
    this.height = 0; // для прямоугольника
    this.x = undefined; // точка от которой будем закрашивать
    this.y = undefined;  // точка к которой будем закрашивать
    this.painting = false; // флаг, должна ли кисть рисовать или нет
}



Painter.prototype = {
    drawRect: function () {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect( this.x, this.y, this.width, this.height);
    },

    drawLine: function (x, y) {
        this.ctx.strokeStyle = this.color; // цвет линии
        this.ctx.lineWidth = this.radius; // толщина линии
        this.ctx.lineCap = "round"; // Закругленные концы линий

        // если координаты не определены - задаем текущие
        if (this.x == undefined || this.y == undefined) {
            this.x = x;
            this.y = y;
        }

        this.ctx.beginPath(); // отрисовываем путь
        this.ctx.moveTo(this.x, this.y); // передвигаем курсор контекста в нужную точку ничего не рисуя
        this.ctx.lineTo(x, y); // проводим линии к точке, которая нам нужна
        this.ctx.stroke(); // отрисовываем линию

        // сохраняем переданные координаты как текущие
        // следующая часть линии будет отрисовываться из последней отрисованной точки - делаю ломаную
        this.x = x;
        this.y = y;
    }, 
    move: function (x, y) {
        this.x = x;
        this.y = y;
    },

    start: function () {
        this.painting = true;
    },

    stop: function () {
        this.painting = false;
        this.x = undefined;
        this.y = undefined;
    }
}


function Caint(canvas) {
    var ctx = canvas.getContext("2d"); // контекст который будет использоваться кистями

    var backgroundColor = "#f5f5f5"; // цвет бэкграунда
    var foregroundColor = "#333"; // цвет кисти

    var painter = new Painter(ctx);
    painter.radius = 3;
    painter.color = foregroundColor;

    var cancel = function (e) {
        e.preventDefault();
        return false;
    }

    this.upListener = function (e) {
        painter.stop();
    }

    this.downListener = function (e) {
        switch (e.button) {
            case 0:
                painter.start();
                break;
            case 2:
                // clear
                break;
        }

        this.draw(e);
        return false;

    }.bind(this);


    this.moveListener = function (e) {
        this.draw(e);
    }.bind(this);

    // преобразуем глобальные координаты окна в локальные координаты канваса
    this.draw = function (e) {
        var x = e.pageX;
        var y = e.pageY;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;

        if (painter.painting) {
            painter.drawLine(x, y);
        }
    }


    canvas.addEventListener('mousedown', this.downListener);
    document.addEventListener('mouseup', this.upListener);
    canvas.addEventListener('mousemove', this.moveListener);
    canvas.addEventListener('contextmenu', cancel);
    canvas.addEventListener('selectstart', cancel);

    document.body.style.color = foregroundColor;

    var background = new Image();
    background.src = "../img/background_snow.jpg";

    ctx.drawImage(background,0,0);

}

new Caint(document.getElementById("canvas"));