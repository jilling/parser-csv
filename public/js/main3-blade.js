function Painter(ctx) {
    this.ctx = ctx;
    this.radius = 0;
    this.width = 0;
    this.height = 0;
    this.color = "#000";
    this.x = undefined;
    this.y = undefined;
    this.overlay = null;
    this.preview = false;
    this.painting = false;
}

Painter.prototype = {
    drawRect: function() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
    },
    previewRect: function() {
        this.clearPreview();
        this.overlay.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.preview = true;
    },
    clearPreview: function() {
        if (this.preview) {
            this.overlay.ctx.clearRect(0, 0, this.overlay.width, this.overlay.height);
            this.preview = false;
        }
    },
    line: function(x, y) {
        this.ctx.strokeStyle = this.color;
        this.ctx.lineWidth = this.radius;
        this.ctx.lineCap = "round";
        if (this.x == undefined || this.y == undefined) {
            this.x = x;
            this.y = y;
        }

        this.ctx.beginPath();
        this.ctx.moveTo(this.x, this.y);
        this.ctx.lineTo(x, y);
        this.ctx.stroke();

        this.x = x;
        this.y = y;
    },
    move: function(x, y) {
        this.x = x;
        this.y = y;

    },
    start: function() {
        this.painting = true;
    },
    stop: function() {
        this.clearPreview();
        this.painting = false;
        this.x = undefined;
        this.y = undefined;
    }
};

function Caint(canvas) {
    var ctx = canvas.getContext("2d");
    var form = document.getElementById('form-controls'); // createElement('form');

    var background = new Image();
    background.src = "/img/back1-bg.jpg";
    //background.src = "/img/fon2-mini.png";



    var logo = new Image();
    logo.src = "/img/new-chris-tree-mini.png";
    //logo.src = "/img/replace7-mini.png";

    var logo2 = new Image();
    logo2.src = "/img/logo-blue-mini.png";


    // хз какого хера сразу второе изображение не подгружается





    var painter = new Painter(ctx);
    painter.radius = 2;//$("input[name=painterRadius]").val(); //form.elements["painterRadius"].value;
    //painter.radius = 2;
    painter.color = "#0c2b3d";//$(".colorpicker_hex input").val();
    //painter.color = '#00597b';



    function cancel(e) {
        e.preventDefault();
        return false;
    }

    this.upListener = function(e) {
        painter.stop();
    };

    this.downListener = function(e) {
        switch(e.button) {
            case 0:
                painter.color = "#0c2b3d"; //"#"+$(".colorpicker_hex input").val();//form.painterColor.value;
                painter.radius = 2;//$("input[name=painterRadius]").val();//form.painterRadius.value;
                painter.start();
                break;
        }
        this.draw(e);

        return false;
    }.bind(this);


    this.draw = function(e) {
        var x = e.pageX;
        var y = e.pageY;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;
        //console.log("x="+x+"; y="+y); // координаты
        if (painter.painting) {
            painter.line(x, y);

        }


    };

    this.moveListener = function(e) {
        this.draw(e);
    }.bind(this);

    $('.print').on('click', function () {
        ctx.font="30px Poiret One"; //#e1e8ef
        ctx.fillStyle = '#0c2b3d';
        printText(ctx, $('textarea[name=giftText]').val(), 50, 190, 600, 35);
    });

    $('.new').on('click', function () {
        ctx.clearRect(0, 0, 640, 480);
        ctx.drawImage(background, 0, 0, 1024, 768);
        //ctx.drawImage(logo, 0, 100, 400, 359);
        ctx.drawImage(logo, 30, 500, 250, 250);
        //ctx.drawImage(logo2, 724, -40, 300, 212);
        ctx.drawImage(logo2, 0, -40, 300, 212);


        $('.new-text').show('slow');

    });

    $('.add-new-text').on('click', function () {
        ctx.font="25px Poiret One"; //#e1e8ef
        ctx.fillStyle = '#0c2b3d';

        var text = document.getElementById('new-text').value.split('\n');


        var startMargin = 250;
        var endMargin = 0;

        $.each(text, function( index, value ) {

            if (index == 0) {
                endMargin = printText(ctx, value, 50, startMargin, 900, 35);
            }

            if (index > 0) {
                endMargin = parseInt(endMargin) + 70;
                endMargin = printText(ctx, value, 50, endMargin, 900, 35);
            }
        });

        //printText(ctx, $('textarea[name=newGiftText]').val(), 50, 250, 800, 35);
    });


    $('.restart').on('click', function () {
        ctx.clearRect(0, 0, 1024, 768);
        setTimeout(function () {
            ctx.drawImage(background,0,0, 1024, 768);

        }, 60);
        setTimeout(function () {
            //ctx.drawImage(logo, 0, 100, 400, 359);
            ctx.drawImage(logo, 30, 500, 250, 250);

        }, 60);
        setTimeout(function () {
            ctx.drawImage(logo2,724,-40, 300, 212);
            //ctx.drawImage(logo2,0,-40, 300, 212);

        }, 60);

        setTimeout(function () {
            ctx.font="25px Poiret One"; //#e1e8ef
            ctx.fillStyle = '#0c2b3d';
            /*var text1 = "Дорогі колеги!";
             printText(ctx, text1, 470, 100, 530, 25);*/
            /*var text2 = 'Через пару днів ми святкуємо Різдво та Новий рік, тому я хотів би скористатися цією можливістю, щоб сказати дякую та поділитися з вами певними побажаннями.';
            printText(ctx, text2, 380, 350, 630, 25);
            var text5 = "Я переконаний, що 2018 рік стане роком, в якому ми будемо працювати не тільки і прагнемо до наших амбітних цілей, але також будемо насолоджуватися цим разом, відчувши владу та задоволення ОДНОЇ КОМАНДИ!";
            printText(ctx, text5, 200, 450, 800, 25);
            var text6 = "З особистого боку, я просто хочу всім вам розслаблятися, спокійні моменти разом з коханою родиною та друзями.";
            printText(ctx, text6, 20, 550, 1000, 25);
            var text7 = "З Різдвом Христовим та з Новим роком!";
            printText(ctx, text7, 620, 650, 530, 25);*/

            var text2 = 'Цьогоріч ми реалізували чимало яскравих, важливих проектів і з натхненням відкриваємо нову сторінку спільної історії. Бажаємо, щоб у новому році здійснилися Ваші найсміливіші плани, а прибутки в бізнесі перевершили очікування. Нехай у вашій оселі будуть затишок, здоров’я та любов – адже немає нічого важливішого за щасливу родину.';
            printText(ctx, text2, 50, 250, 800, 35);
            var text3 = 'За традицією ми не даруємо новорічних подарунків нашим партнерам. Натомість щороку виступаємо з новою соціальною ініціативою. Завдяки Вам в 2017 році PFG Ukraine закупила обладнання для дитячої лікарні Охматдит. Тож сподіваємося, безмежна вдячність батьків і щасливі усмішки малюків - це найкращий новорічний подарунок для Вас.';
            printText(ctx, text3, 160, 450, 800, 35);
            /*var text4 = 'З Новим роком та Різдвом Христовим Вас!';
            printText(ctx, text4, 350, 635, 580, 35);*/

            var text5 = 'З найкращими побажаннями,';
            printText(ctx, text5, 630, 650, 530, 35);


            /*var text6 = 'команда Porsche Finance Group Ukraine';
            printText(ctx, text6, 530, 680, 530, 35);*/
            var text6 = 'Йоахім Арер';
            printText(ctx, text6, 630, 680, 530, 35);
            var text7 = 'Директор PFG Ukraine';
            printText(ctx, text7, 630, 715, 530, 35);
        }, 60);

    });

    canvas.addEventListener("mousedown", this.downListener);
    document.addEventListener("mouseup", this.upListener);
    canvas.addEventListener("mousemove", this.moveListener);
    canvas.addEventListener("contextmenu", cancel);
    document.addEventListener("selectstart", cancel);

    /*setTimeout(function () {
        $('.restart').click();
    }, 60);*/


    ctx.drawImage(background,0,0, 1024, 768);

    //ctx.drawImage(logo, 0, 100, 400, 359);
    ctx.drawImage(logo, 30, 500, 250, 250);

    ctx.drawImage(logo2,724,-40, 300, 212);
    //ctx.drawImage(logo2,0,-40, 300, 212);

    ctx.font="25px Poiret One"; //#e1e8ef
    ctx.fillStyle = '#0c2b3d';
    /*var text1 = "Дорогі колеги!";
     printText(ctx, text1, 470, 100, 530, 25);*/
    var text2 = 'Цьогоріч ми реалізували чимало яскравих, важливих проектів і з натхненням відкриваємо нову сторінку спільної історії. Бажаємо, щоб у новому році здійснилися Ваші найсміливіші плани, а прибутки в бізнесі перевершили очікування. Нехай у вашій оселі будуть затишок, здоров’я та любов – адже немає нічого важливішого за щасливу родину.';
    printText(ctx, text2, 50, 250, 800, 35);
    var text3 = 'За традицією ми не даруємо новорічних подарунків нашим партнерам. Натомість щороку виступаємо з новою соціальною ініціативою. Завдяки Вам в 2017 році PFG Ukraine закупила обладнання для дитячої лікарні Охматдит. Тож сподіваємося, безмежна вдячність батьків і щасливі усмішки малюків - це найкращий новорічний подарунок для Вас.';
    printText(ctx, text3, 160, 450, 800, 35);
    /*var text4 = 'З Новим роком та Різдвом Христовим Вас!';
     printText(ctx, text4, 350, 635, 580, 35);*/

    var text5 = 'З найкращими побажаннями,';
    printText(ctx, text5, 630, 650, 530, 35);
    /*var text6 = 'команда Porsche Finance Group Ukraine';
    printText(ctx, text6, 530, 680, 530, 35);*/
    var text6 = 'Йоахім Арер';
    printText(ctx, text6, 630, 680, 530, 35);
    var text7 = 'Директор PFG Ukraine';
    printText(ctx, text7, 630, 715, 530, 35);

    /*var text3 = "З професійної сторони ми зараз залишаємо жорсткий рік, але ми все ж таки обробляємось дуже професійно та ефективно, перетворюючи труднощі на виклики, з якими ми стикаємося, і хороші результати, які ми досягли, є ясним доказом того, що нам вдалося!";
     printText(ctx, text3, 470, 250, 530, 25);
     var text4 = "Дозвольте мені висловити щиру подяку кожному з вас за рішуче, постійне зобов'язання до наших спільних цілей, а також за особисту відданість вашій роботі!";
     printText(ctx, text4, 470, 400, 530, 25);*/
    /*var text5 = "Я переконаний, що 2018 рік стане роком, в якому ми будемо працювати не тільки і прагнемо до наших амбітних цілей, але також будемо насолоджуватися цим разом, відчувши владу та задоволення ОДНОЇ КОМАНДИ!";
    printText(ctx, text5, 200, 450, 800, 25);
    var text6 = "З особистого боку, я просто хочу всім вам розслаблятися, спокійні моменти разом з коханою родиною та друзями.";
    printText(ctx, text6, 20, 550, 1000, 25);
    var text7 = "З Різдвом Христовим та з Новим роком!";
    printText(ctx, text7, 620, 650, 530, 25);*/

}


function printText(context, text, marginLeft, marginTop, maxWidth, lineHeight) {
    var words = text.split(" ");
    var countWords = words.length;
    var line = "";
    for (var n = 0; n < countWords; n++) {
        var testLine = line + words[n] + " ";

        /*if (words[n] == '2017') {
            context.font="20px centurygothic";
        } else {
            context.font="25px Poiret One";
        }*/

        var testWidth = context.measureText(testLine).width;
        if (testWidth > maxWidth) {
            context.fillText(line, marginLeft, marginTop);
            line = words[n] + " ";
            marginTop += lineHeight;
        }
        else {
            line = testLine;
        }
    }


    context.fillText(line, marginLeft, marginTop);

    return marginTop;
}

$('#colorSelector').ColorPicker({
    color: '#333',
    onShow: function (colpkr) {
        $(colpkr).fadeIn(500);
        return false;
    },
    onHide: function (colpkr) {
        $(colpkr).fadeOut(500);
        return false;
    },
    onChange: function (hsb, hex, rgb) {
        $('#colorSelector div').css('backgroundColor', '#' + hex);
        $('input[name=painterColor]').val('#' + hex);
    }
});

    /// snow
    var imageDir = "http://mvcreative.ru/example/6/2/snow/";
    var sflakesMax = 65;
    var sflakesMaxActive = 65;
    var svMaxX = 2;
    var svMaxY = 6;
    var ssnowStick = 1;
    var ssnowCollect = 0;
    var sfollowMouse = 1;
    var sflakeBottom = 0;
    var susePNG = 1;
    var sflakeTypes = 5;
    var sflakeWidth = 15;
    var sflakeHeight = 15;


$(document).ready(function(){
    new Caint(document.getElementById("canvas"));

    $('.plus.radius').on('click', function () {
        $('input[name=painterRadius]').val( parseInt($('input[name=painterRadius]').val())+1);
    });

    $('.minus.radius').on('click', function () {
        if ($('input[name=painterRadius]').val() > 1) {
            $('input[name=painterRadius]').val($('input[name=painterRadius]').val()-1);
        }

    });

    $('.plus.font-size').on('click', function () {
        $('input[name=fontSize]').val( parseInt($('input[name=fontSize]').val())+1);
    });

    $('.minus.font-size').on('click', function () {
        if ($('input[name=fontSize]').val() > 1) {
            $('input[name=fontSize]').val($('input[name=fontSize]').val()-1);
        }

    });

    $('.jpeg').on('click', function () {
        /*var img = $("#canvas")[0].toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/, '');

        $.ajax({
            type: "POST",
            url: '/jpeg',
            data: {img: img},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( msg ) {
                alert(msg);
            }
        });*/


        var canvas = document.getElementById("canvas");

        var d = new Date();
        var n = d.getTime();

        canvas.toBlob(function(blob) {
            saveAs(blob, n+".png");
        });
    });


    $('.gif').on('click', function () {
        var img = $("#canvas")[0].toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/, '');
        $('#mask-load').css({'width':$(window).width(),'height':$(document).height()});


        //$('#mask').fadeIn(1000);
        $('#mask-load').fadeTo("slow",0.8);
        $.ajax({
            type: "POST",
            url: '/gif',
            data: {'img': img, 'lang': 'ua'},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( id ) {
                /*var a  = document.createElement('a');
                a.href = img;
                a.download = img;
                a.click();
                a.remove();*/

                //$('#savingImages').attr({href: img}).click();


                window.location.href = "/images/"+id;

                $('#mask-load').hide('slow');
            }
        });

    });


    $('.send-email').on('click', function () {

        var email = $('input[name=email]').val();

        $('.error-message').hide();
        $('.ok-message').hide();

        $('input[name=email]').removeClass('error-input').removeClass('ok-input');
        if (validateEmail(email)) {



            $('#mask-load').css({'width':$(window).width(),'height':$(document).height()});
            $('#mask-load').fadeTo("slow",0.8);

            var img = $("#canvas")[0].toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/, '');
            var type = $('input[name=image-type]:checked').val();
            $.ajax({
                type: "POST",
                url: '/send-mail',
                data: {'email': email, 'img': img, 'type': type, 'lang': 'ua'},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function( ) {
                    $('#mask-load').hide('slow');
                    $('input[name=email]').addClass('ok-input');
                    $('.ok-message').show();
                }
            });
        } else {
            $('input[name=email]').addClass('error-input');
            $('.error-message').show();
        }

    });

    $('.save').click( function(event){
        event.preventDefault();
        $('#overlay').fadeIn(400, // анимируем показ обложки
            function(){ // далее показываем мод. окно
                $('#modal_form')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '50%'}, 200);
            });
    });

    $('.send').click( function(event){
        event.preventDefault();
        $('#overlay').fadeIn(400, // анимируем показ обложки
            function(){ // далее показываем мод. окно
                $('#modal_form_send')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '50%'}, 200);
            });
    });

    // закрытие модального окна
    $('#modal_close, #overlay').click( function(){
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,  // уменьшаем прозрачность
                function(){ // пoсле aнимaции
                    $(this).css('display', 'none'); // скрываем окно
                    $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );

        $('#modal_form_send')
            .animate({opacity: 0, top: '45%'}, 200,  // уменьшаем прозрачность
                function(){ // пoсле aнимaции
                    $(this).css('display', 'none'); // скрываем окно
                    $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );
    });

});
// для корректной загрузки
window.onload = function() {
    $('.restart').click();
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function Download(url) {
    document.getElementById('my_iframe').src = url;
}