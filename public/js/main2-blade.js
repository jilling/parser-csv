function Painter(ctx) {
    this.ctx = ctx;
    this.radius = 0;
    this.width = 0;
    this.height = 0;
    this.color = "#000";
    this.x = undefined;
    this.y = undefined;
    this.overlay = null;
    this.preview = false;
    this.painting = false;
}

Painter.prototype = {
    drawRect: function() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
    },
    previewRect: function() {
        this.clearPreview();
        this.overlay.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.preview = true;
    },
    clearPreview: function() {
        if (this.preview) {
            this.overlay.ctx.clearRect(0, 0, this.overlay.width, this.overlay.height);
            this.preview = false;
        }
    },
    line: function(x, y) {
        this.ctx.strokeStyle = this.color;
        this.ctx.lineWidth = this.radius;
        this.ctx.lineCap = "round";
        if (this.x == undefined || this.y == undefined) {
            this.x = x;
            this.y = y;
        }

        this.ctx.beginPath();
        this.ctx.moveTo(this.x, this.y);
        this.ctx.lineTo(x, y);
        this.ctx.stroke();

        this.x = x;
        this.y = y;
    },
    move: function(x, y) {
        this.x = x;
        this.y = y;
    },
    start: function() {
        this.painting = true;
    },
    stop: function() {
        this.clearPreview();
        this.painting = false;
        this.x = undefined;
        this.y = undefined;
    }
};

function Caint(canvas) {
    var ctx = canvas.getContext("2d");
    var form = document.getElementById('form-controls'); // createElement('form');

    var background = new Image();
    background.src = "/img/back2-bg.jpg";

    var logo = new Image();
    logo.src = "/img/giftcard-rotation.png";




    // хз какого хера сразу второе изображение не подгружается
    setTimeout(function () {
        ctx.drawImage(background,0,0, 1024, 768);
        setTimeout(function () {
            ctx.drawImage(logo,0,0, 400, 359);
        }, 30);
    }, 30);

    var backgroundColor = "#f5f5f5";

    var painter = new Painter(ctx);
    //painter.radius = form.elements["painterRadius"].value;
    painter.radius = 2;
    //painter.color = $(".colorpicker_hex input").val();
    painter.color = '#00597b';

    var erasingPreview = false;
    var eraser = new Painter(ctx);
    eraser.width = 10;
    eraser.height = 10;
    eraser.color = backgroundColor;

    function cancel(e) {
        e.preventDefault();
        return false;
    }

    this.upListener = function(e) {
        eraser.stop();
        painter.stop();
    };

    this.downListener = function(e) {
        switch(e.button) {
            case 0:
                /*painter.color = form.painterColor.value;
                painter.radius = form.painterRadius.value;*/
                painter.start();
                break;
        }
        this.draw(e);

        return false;
    }.bind(this);


    this.draw = function(e) {
        var x = e.pageX;
        var y = e.pageY;

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;

        if (painter.painting) {
            painter.line(x, y);
        } else if (eraser.painting) {
            eraser.move(x - eraser.width / 2, y - eraser.height / 2);
            eraser.previewRect();
            eraser.drawRect();
        }

    };

    this.moveListener = function(e) {
        this.draw(e);
    }.bind(this);

    $('.print').on('click', function () {
        ctx.font="20px Arial"; //#e1e8ef
        ctx.fillStyle = '#e1e8ef';
        printText(ctx, $('textarea[name=giftText]').val(), 145, 205, 150, 25);
    });

    $('.restart').on('click', function () {
        ctx.clearRect(0, 0, 640, 480);
        ctx.drawImage(background,0,0, 1024, 768);
        ctx.drawImage(logo,0,0, 400, 359);
    });

    canvas.addEventListener("mousedown", this.downListener);
    document.addEventListener("mouseup", this.upListener);
    canvas.addEventListener("mousemove", this.moveListener);
    canvas.addEventListener("contextmenu", cancel);
    document.addEventListener("selectstart", cancel);


}


function printText(context, text, marginLeft, marginTop, maxWidth, lineHeight) {
    var words = text.split(" ");
    var countWords = words.length;
    var line = "";
    for (var n = 0; n < countWords; n++) {
        var testLine = line + words[n] + " ";
        var testWidth = context.measureText(testLine).width;
        if (testWidth > maxWidth) {
            context.fillText(line, marginLeft, marginTop);
            line = words[n] + " ";
            marginTop += lineHeight;
        }
        else {
            line = testLine;
        }
    }
    context.fillText(line, marginLeft, marginTop);
}

$('#colorSelector').ColorPicker({
    color: '#333',
    onShow: function (colpkr) {
        $(colpkr).fadeIn(500);
        return false;
    },
    onHide: function (colpkr) {
        $(colpkr).fadeOut(500);
        return false;
    },
    onChange: function (hsb, hex, rgb) {
        $('#colorSelector div').css('backgroundColor', '#' + hex);
        $('input[name=painterColor]').val('#' + hex);
    }
});

    /// snow
    var imageDir = "http://mvcreative.ru/example/6/2/snow/";
    var sflakesMax = 65;
    var sflakesMaxActive = 65;
    var svMaxX = 2;
    var svMaxY = 6;
    var ssnowStick = 1;
    var ssnowCollect = 0;
    var sfollowMouse = 1;
    var sflakeBottom = 0;
    var susePNG = 1;
    var sflakeTypes = 5;
    var sflakeWidth = 15;
    var sflakeHeight = 15;


$(document).ready(function(){
    new Caint(document.getElementById("canvas"));

    $('.plus.radius').on('click', function () {
        $('input[name=painterRadius]').val( parseInt($('input[name=painterRadius]').val())+1);
    });

    $('.minus.radius').on('click', function () {
        if ($('input[name=painterRadius]').val() > 1) {
            $('input[name=painterRadius]').val($('input[name=painterRadius]').val()-1);
        }

    });

    $('.plus.font-size').on('click', function () {
        $('input[name=fontSize]').val( parseInt($('input[name=fontSize]').val())+1);
    });

    $('.minus.font-size').on('click', function () {
        if ($('input[name=fontSize]').val() > 1) {
            $('input[name=fontSize]').val($('input[name=fontSize]').val()-1);
        }

    });

    $('.jpeg').on('click', function () {
        /*var img = $("#canvas")[0].toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/, '');

        $.ajax({
            type: "POST",
            url: '/jpeg',
            data: {img: img},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( msg ) {
                alert(msg);
            }
        });*/


        var canvas = document.getElementById("canvas");

        var d = new Date();
        var n = d.getTime();

        canvas.toBlob(function(blob) {
            saveAs(blob, n+".png");
        });
    });


    $('.gif').on('click', function () {
        var img = $("#canvas")[0].toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/, '');
        /*$('#mask').css({'width':$(window).width(),'height':$(document).height()});


        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);*/
        $.ajax({
            type: "POST",
            url: '/gif',
            data: {img: img},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( id ) {
                // reload! /images/(id)
                //window.location.href = "/images/"+id;
                console.log(id);
            }
        });

    });
});



/*$(document).ready(function() {
    $('a[name=modal]').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('href');

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({'width':maskWidth,'height':maskHeight});

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        $(id).fadeIn(2000);

    });

    $('.window .close').click(function (e) {
        e.preventDefault();
        $('#mask, .window').hide();
    });

    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });

});*/
