<?php

namespace App\Http\Controllers;

use App\Http\Controllers\GifCreator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Hierarchy;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\GifCreator;

class HierarchyController extends Controller
{
    public function index () {

        return view('hierarchy');
    }

    function createCycle ($arBosses, $currentPosition, $endPosition) {

        if ($currentPosition > $endPosition) {
            return false;
        }

        $data = array();

        // кол-во подчиненных у каждого босса (по уровням)
        $arPosition = array(
            "2"=>array(
                "employee" => 3,
                "salary" => 8000,
            ),
            "3"=>array(
                "employee" => 3,
                "salary" => 6000,
            ),
            "4"=>array(
                "employee" => 5,
                "salary" => 4000,
            ),
            "5"=>array(
                "employee" => 15,
                "salary" => 2000,
            )
        );


        // для каждого босса из массива записываем новых сотрудников
        foreach ( $arBosses as $boss) {

            for ($i = 1; $i < $arPosition[$currentPosition]["employee"]; $i++) {

                $data[] = array(
                    "name" => 'Employee #'.$i.' Level '.$currentPosition,
                    "position" => $currentPosition,
                    "start" => Carbon::now(),
                    "salary" => $arPosition[$currentPosition]["salary"],
                    "id_parent" => $boss,
                );

            }

            // записываем всех новых сотрудников для данного босса
            \App\Hierarchy::insert($data);

        }

        // выбрать всех только что добавленных сотрудников для передачи в функцию цикла добавления

        $newList = DB::table('hierarchy')->select('id')->where('position', $currentPosition)->get();

        $newBosses = array();

        foreach ($newList as $value) {
            $newBosses[] = $value->id;
        }

        $currentPosition++;

        $this->createCycle($newBosses, $currentPosition, $endPosition);

    }

    public function createHierarchy () {

        // создать первого

        $boss = new Hierarchy();
        $boss->name = 'Boss';
        $boss->position = '1';
        $boss->start =  Carbon::now();
        $boss->salary = 10000;
        $boss->id_parent = 0;
        $boss->save();

        $this->createCycle([$boss->id], 2, 5);
    }

    public function createJpeg() {
        $name = time().'.jpeg';
        file_put_contents('created_images/'.$name, base64_decode($_POST['img']));

        // Добавить в базу

    }

    public function createGif() {


        $name = time();
        file_put_contents('created_images/'.$name.'.jpeg', base64_decode($_POST['img']));

        // сделать гифку
        if ($_POST['lang'] == 'ua') {
            $frames = array(
                imagecreatefromjpeg('img/gif1-ukr-mini.jpg'),
                imagecreatefromjpeg('created_images/'.$name.'.jpeg'),
                imagecreatefromjpeg('img/gif3-ukr-mini.jpg')
            );
        } else {
            $frames = array(
                imagecreatefromjpeg('img/gif1-eng-mini.jpg'),
                imagecreatefromjpeg('created_images/'.$name.'.jpeg'),
                imagecreatefromjpeg('img/gif3-eng-mini.jpg')
            );
        }

        $durations = array(150, 400, 150);



        $gc = new GifCreator();
        $gc->create($frames, $durations, 0);
        // сохранить гифку
        $gifBinary = $gc->getGif();
        file_put_contents('created_images/'.$name.'.gif', $gifBinary);
        // Добавить путь в базу

        $id = DB::table('pictures')->insertGetId(
            ['url' => 'created_images/'.$name.'.gif']
        );

        //return 'created_images/'.$name.'.gif';
        return $id;
    }

    public function sendMail () {

        //echo "mail";
        //dd($_SERVER);

        $name = time();
        file_put_contents('created_images/'.$name.'.jpeg', base64_decode($_POST['img']));


        if ($_POST['type'] == 'gif') {
            // сделать гифку
            if ($_POST['lang'] == 'ua') {
                $frames = array(
                    imagecreatefromjpeg('img/gif1-ukr-mini.jpg'),
                    imagecreatefromjpeg('created_images/'.$name.'.jpeg'),
                    imagecreatefromjpeg('img/gif3-ukr-mini.jpg')
                );
            } else {
                $frames = array(
                    imagecreatefromjpeg('img/gif1-eng-mini.jpg'),
                    imagecreatefromjpeg('created_images/'.$name.'.jpeg'),
                    imagecreatefromjpeg('img/gif1-eng-mini.jpg')
                );
            }

            $durations = array(150, 400, 150);

            $gc = new GifCreator();
            $gc->create($frames, $durations, 0);
            // сохранить гифку
            $gifBinary = $gc->getGif();

            $nameGif = time() +1;
            file_put_contents('created_images/'.$nameGif.'.gif', $gifBinary);
            // Добавить путь в базу

            /*$id = DB::table('pictures')->insertGetId(
                ['url' => 'created_images/'.$nameGif.'.gif']
            );*/
        }

        //$to = 'evgeniy.korsun@gmail.com';
        $to = $_POST['email'];
        $subject="Merry Christmas and a Happy New Year";
        $from = 'Porsche Finance Group Ukraine <NY@pfg.ua>';
        $body = '<html><body>';

        if ($_POST['type'] == 'gif') {
            $body .= '<a href="http://'.$_SERVER['SERVER_NAME'].'"><img src="http://'.$_SERVER['SERVER_NAME'].'/created_images/'.$nameGif.'.gif" alt="gif image" /></a><br/>';
        } else {
            $body .= '<a href="http://'.$_SERVER['SERVER_NAME'].'"><img src="http://'.$_SERVER['SERVER_NAME'].'/created_images/'.$name.'.jpeg" alt="gif image" /></a><br/>';
        }

        $body .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
        //$body .= '<p style="color:#3366FF;font-size:14px;">Hello'.',</p>';
        $body .= "</table>";
        $body .= "</body></html>";

        $headers = "From: $from \r\n";
        $headers .= "Reply-To: $$from \r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n" .

            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $body,$headers);
    }


    public function parseMail () {

        $row = 1;
        if (($handle = fopen("12-email.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                echo $data[0] . " == ".$data[1]."<br/>";

                // вставляем в картинку текст и отправляем на почту

                $img="img/new-back-for-emails-eng.png";
                $pic = imagecreatefrompng($img); //открываем рисунок в формате JPEG
                header("Content-type: image/png"); //указываем на тип передаваемых данных
                $color=imagecolorallocate($pic, 12, 43, 61); //получаем идентификатор цвета
                /* определяем место размещения текста по вертикали и горизонтали */
                $h = 190; //высота
                $w = 50; //ширина
                /* выводим текст на изображение */
                imagettftext($pic, 25, 0, $w, $h, $color, "font/po.ttf", $data[0]);
                $name = time();

                imagepng($pic,"created_images/".$name.".png"); //сохраняем рисунок в формате JPEG

                /////////

                $frames = array(
                    imagecreatefromjpeg('img/gif3-eng-mini.jpg'),
                    imagecreatefrompng('created_images/'.$name.'.png'),
                    imagecreatefromjpeg('img/gif1-eng-mini.jpg')
                );

                $durations = array(100, 400, 100);

                $gc = new GifCreator();
                $gc->create($frames, $durations, 0);
                // сохранить гифку
                $gifBinary = $gc->getGif();

                $nameGif = time()+1;
                file_put_contents('created_images/'.$nameGif.'.gif', $gifBinary);
                // Добавить путь в базу

                /*$id = DB::table('pictures')->insertGetId(
                    ['url' => 'created_images/'.$nameGif.'.gif']
                );*/


                //$to = 'evgeniy.korsun@gmail.com';
                $to = $data[1];
                $subject="Merry Christmas and a Happy New Year";
                $from = 'Porsche Finance Group Ukraine <NY@pfg.ua>';
                $body = '<html><body>';

                $body .= '<a href="http://'.$_SERVER['SERVER_NAME'].'"><img src="http://'.$_SERVER['SERVER_NAME'].'/created_images/'.$nameGif.'.gif" alt="gif image" /></a><br/>';

                $body .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                //$body .= '<p style="color:#3366FF;font-size:14px;">Hello'.',</p>';
                $body .= "</table>";
                $body .= "</body></html>";

                $headers = "From: $from \r\n";
                $headers .= "Reply-To: $$from \r\n";
                $headers .= 'MIME-Version: 1.0' . "\r\n" .
                    'Content-type: text/html; charset=iso-8859-1' . "\r\n" .

                    'X-Mailer: PHP/' . phpversion();

                mail($to, $subject, $body,$headers);




            }
            fclose($handle);
        }
    }

    public function sendMailTest () {

        echo "mail test";
        //dd($_SERVER);

        //$name = time();
        //file_put_contents('created_images/'.$name.'.jpeg', base64_decode($_POST['img']));

        $to = 'evgeniy.korsun@gmail.com';
        //$to = "olessja.kosarjuk@porschefinance.ua, liliia.navrotska@porschefinance.ua,";
        //$to .= "kosarjuk@ukr.net, l.a.navrotskaya@gmail.com, evgeniy.korsun@gmail.com";
        $subject="Merry Christmas and a Happy New Year";
        $from = 'Porsche Finance Group Ukraine <NY@pfg.ua>';
        $body = '<html><body>';

        $body .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
        $body .= '<p style="color:#3366FF;font-size:14px;">Тут может быть какой то текст'.',</p>';
        $body .= "</table>";
        $body .= "</body></html>";

        //$body .= '<a href="http://'.$_SERVER['SERVER_NAME'].'"><img src="http://'.$_SERVER['SERVER_NAME'].'/img/olesya.png" alt="new year" /></a><br/>';
        $body .= '<a href="http://'.$_SERVER['SERVER_NAME'].'"><img src="http://'.$_SERVER['SERVER_NAME'].'/img/olesya.gif" alt="new year" /></a><br/>';
        //$body .= '<img src="http://'.$_SERVER['SERVER_NAME'].'/img/1A-mini.jpg" alt="new year" /><br/>';
        //$body .= '<p style="color:#3366FF;font-size:14px;">Hello'.',</p>';
        $body .= "</body></html>";

        $headers = "From: $from \r\n";
        $headers .= "Reply-To: $$from \r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n" .

            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $body,$headers);
    }

    public function showImages ($id) {

        $table = DB::table('pictures')->find($id);


        return view('images', compact('table'));
    }
}
