<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hierarchy extends Model
{

    protected $table = 'hierarchy';
//    protected $guarded = [];
    protected $fillable = ['name', 'position', 'start', 'salary', 'id_parent', 'updated_at', 'created_at'];

}
