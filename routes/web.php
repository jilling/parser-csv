<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome3');
});

Route::get('/var2', function () {
    return view('welcome2');
});

Route::get('/var3', function () {
    return view('welcome');
});

Route::get('/hr', 'HierarchyController@createHierarchy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/jpeg', 'HierarchyController@createJpeg');

Route::post('/gif', 'HierarchyController@createGif');
//Route::get('/gif', 'HierarchyController@createGif');

Route::get('/images/{id}', 'HierarchyController@showImages');
Route::get('eng/images/{id}', 'HierarchyController@showImages');
Route::get('/eng', function () {
    return view('welcome3-eng');
});

Route::get('/test-mail', 'HierarchyController@sendMailTest');
Route::post('/test-mail', 'HierarchyController@sendMailTest');

Route::get('/send-mail', 'HierarchyController@sendMail');
Route::post('/send-mail', 'HierarchyController@sendMail');

Route::get('/parser', 'HierarchyController@parseMail');
Route::post('/parser', 'HierarchyController@parseMail');