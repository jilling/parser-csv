<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Porsche gift card</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL::asset('css/colorpicker.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/layout.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/style.css')}}" />
</head>
<body>



<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 logo">
                <img src="{{URL::asset('img/logo-blue-mini.png')}}">
            </div>
        </div>
        {{--<div class="row">
            <div class="col-sm-12">
                <h1 style="
    margin-bottom: 15px;
    margin-top: 15px;
">Создание Новогодней открытки</h1>
            </div>
        </div>--}}

        <div class="row">
            <div class="col-sm-8" style="position: inherit;">
                <canvas id="canvas" width="640" height="480">
                    <img src="{{URL::asset('img/background640.jpg')}}">
                </canvas>
            </div>
            <div class="col-sm-4">
                <form class="form-controls" id="form-controls">

                    {{--<div class="form-row">
                        <div class="row-label" style="padding-top: 10px;">
                            Колір кисті:
                        </div>

                        <div class="row-content">
                            <input name="painterColor" value="#333" hidden>
                            <div id="colorSelector">
                                <div style="background-color: #333"></div>
                            </div>
                        </div>
                    </div>--}}

                    <div class="row-form">
                        <div class="row-label" style="padding-top: 5px;">
                            Розмір кисті:
                        </div>

                        <div class="row-content">
                            <input name="painterColor" value="#333" hidden>
                            <input class="form-control form-input" name="painterRadius" value="3" readonly>
                            <a class="plus radius"><img src="{{URL::asset('img/plus.jpg')}}"></a>
                            <a class="minus radius"><img src="{{URL::asset('img/minus.jpg')}}"></a>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="row-label" style="padding-top: 30px;">
                            Текст на листівці:
                        </div>

                        <div class="row-content">
                            <textarea class="form-control" rows="4" name="giftText">Щасливого Нового року та веселого Різдва</textarea>
                        </div>
                    </div>
                </form>
            </div>


        </div>

        <div class="row">
            <div class="col-sm-12">
                    <form class="form-controls" id="form-controls">

                        {{--<div class="form-row">
                            <div class="row-label" style="padding-top: 10px;">
                                Колір кисті:
                            </div>

                            <div class="row-content">
                                <input name="painterColor" value="#333" hidden>
                                <div id="colorSelector">
                                    <div style="background-color: #333"></div>
                                </div>
                            </div>
                        </div>--}}


                        {{--<div class="row-form">
                            <div class="row-label" style="padding-top: 5px;">
                                Размер шрифта:
                            </div>

                            <div class="row-content">
                                <input class="form-control form-input" name="fontSize" value="14" readonly>

                                <a class="plus font-size"><img src="{{URL::asset('img/plus.jpg')}}"></a>
                                <a class="minus font-size"><img src="{{URL::asset('img/minus.jpg')}}"></a>
                            </div>
                        </div>--}}


                        <div class="row-form" style="display: inline-flex;">
                            <div>
                                <a class="btn btn-lg btn-primary restart">Оновити листівку</a>
                            </div>
                            <div style="padding-left: 10px;">
                                <a class="btn btn-lg btn-primary print">Додати привітання</a>
                            </div>

                        </div>

                        <div class="row-form" style="display: inline-flex;">
                            <div>
                                <a class="btn btn-lg btn-primary jpeg">Зберегти JPEG</a>
                            </div>
                            <div style="padding-left: 10px;">
                                <a class="btn btn-lg btn-primary gif">Зберегти Gif</a>
                            </div>

                        </div>

                        <div class="row-form" style="display: inline-flex;">
                            <div>
                                <a class="btn btn-lg btn-primary jpeg">Отримати посилання</a>
                            </div>
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>
<div id="mask"><img src="{{URL::asset('img/loading_apple.gif')}}"></div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->




<script type="text/javascript" src="{{URL::asset('js/jquery3.2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/colorpicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/eye.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/utils.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/layout.js?ver=1.0.2')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/blob.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/fileSaver.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/main2.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/snow.js')}}"></script>
</body>
</html>
