<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Новорічна сторінка привітань від PFG Ukraine</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL::asset('css/colorpicker.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/layout.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/style.css')}}" />
</head>
<body>
<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
<style>

    @font-face {
        font-family: 'Lobster2';
        font-style: normal;
        font-weight: 400;
        src: local('Lobster Two'), local('LobsterTwo'), url({{URL::asset('/font/lobster2.woff2')}}) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2212, U+2215;
    }

    /*@font-face {
        font-family: 'Lobster';
        font-style: normal;
        font-weight: 400;
        src: local('Lobster Regular'), local('Lobster-Regular'), url({{URL::asset('/font/1.woff2')}}) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }*/

    @font-face {
        font-family: Rosamunda; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/rosamunda-two.ttf')}}); /* Путь к файлу со шрифтом */
    }
    @font-face {
        font-family: Corinthia; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/Corinthia.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: Pompadur; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/Pompadur.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: linoscript; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/linoscript.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: menuet; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/menuet.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: annabelle; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/annabelle.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: esenin; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/esenin.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: decor; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/decor.ttf')}}); /* Путь к файлу со шрифтом */
    }

    @font-face {
        font-family: centurygothic; /* Гарнитура шрифта */
        font-weight: 400;
        src: url({{URL::asset('/font/CenturyGothic.ttf')}}); /* Путь к файлу со шрифтом */
    }


    html, body {
        /*background: url(img/back1-bg.jpg) no-repeat center center fixed !important;*/
        /*font-family: Arial, Helvetica, sans-serif;*/
        font-family: 'Poiret One', Arial, sans-serif;
        font-size: 12px;
        line-height: 18px;
        /*color: #52697E;*/
        color: #0069d9;
        /*-moz-background-size: 100%;
        -webkit-background-size: 100%;
        -o-background-size: 100%;
        background-size: 100%; */

        background-size: cover;
    }
</style>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            <div class="col-12">
                <ul class="lang">
                    <li><a href="/eng" style="color: #00597b;">English version</a></li>
                </ul>
            </div>

        </div>
        <div class="row">
            &nbsp;
        </div>
        {{--<div class="row">
            <div class="col-sm-12 logo">
                <img src="{{URL::asset('img/logo-blue-mini.png')}}">
            </div>
        </div>--}}
        {{--<div class="row">
            <div class="col-sm-12">
                <h1 style="
    margin-bottom: 15px;
    margin-top: 15px;
">Создание Новогодней открытки</h1>
            </div>
        </div>--}}

        <div class="row">
            <div class="col-sm-12" style="position: inherit;font-family: Lobster,Lobster, sans-serif;">
                <canvas id="canvas" width="1024" height="768" style="font-family: Lobster,Lobster, sans-serif;"></canvas>
            </div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            <div class="col-sm-12" style="font-family: Arial, Helvetica, sans-serif;">
                <form class="form-controls" id="form-controls">

                    {{--<div class="form-row">
                        <div class="row-label" style="padding-top: 10px;">
                            Колір кисті:
                        </div>

                        <div class="row-content">
                            <input name="painterColor" value="#333" hidden>
                            <div id="colorSelector">
                                <div style="background-color: #333"></div>
                            </div>
                        </div>
                    </div>--}}


                    {{--<div class="row-form">
                        <div class="row-label" style="padding-top: 5px;">
                            Размер шрифта:
                        </div>

                        <div class="row-content">
                            <input class="form-control form-input" name="fontSize" value="14" readonly>

                            <a class="plus font-size"><img src="{{URL::asset('img/plus.jpg')}}"></a>
                            <a class="minus font-size"><img src="{{URL::asset('img/minus.jpg')}}"></a>
                        </div>
                    </div>--}}


                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary print">Додати ім'я</a>
                        </div>
                    </div>

                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary new-text add-new-text" style="display: none;">Додати текст</a>
                        </div>
                    </div>

                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary restart">Оновити листівку</a>
                        </div>
                    </div>

                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary new">Створити листівку</a>
                        </div>
                    </div>



                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary save" href="#save" name="modal">Зберегти</a>
                        </div>
                        {{--<div style="padding-left: 10px;">
                            <a class="btn btn-lg btn-primary gif">Зберегти Gif</a>
                        </div>--}}

                    </div>

                    <div class="row-form" style="display: inline-flex;">
                        <div>
                            <a class="btn btn-lg btn-primary send">Вiдправити на пошту</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12" style="font-family: Arial, Helvetica, sans-serif;">
                    <form class="form-controls" id="form-controls">

                        {{--<div class="form-row">
                            <div class="row-label" style="padding-top: 10px;">
                                Колір кисті:
                            </div>

                            <div class="row-content">
                                <input name="painterColor" value="#333" hidden>
                                <div id="colorSelector">
                                    <div style="background-color: #333"></div>
                                </div>
                            </div>
                        </div>--}}
                        <input name="painterColor" value="#333" hidden>
                        {{--<div class="row-form">
                            <div class="row-label" style="padding-top: 5px;">
                                Розмір кисті:
                            </div>

                            <div class="row-content">
                                <input name="painterColor" value="#333" hidden>
                                <input class="form-control form-input" name="painterRadius" value="3" readonly>
                                <a class="plus radius"><img src="{{URL::asset('img/plus.jpg')}}"></a>
                                <a class="minus radius"><img src="{{URL::asset('img/minus.jpg')}}"></a>
                            </div>
                        </div>--}}
                        <div class="row-form">
                            <div class="row-label" style="padding-top: 30px;">
                                Iм'я на листівці:
                            </div>

                            <div class="row-content">
                                <textarea class="form-control" rows="4" id="textarea" name="giftText" maxlength="70" placeholder="Введіть ім'я"></textarea>
                            </div>

                            <div class="row-label new-text" style="padding-top: 30px; display: none;">
                                Текст на листівці:
                            </div>
                            <div class="row-content new-text" style="display: none;">
                                <textarea class="form-control" rows="4" cols="80" id="new-text" name="newGiftText" placeholder="Введіть ваше привітання"></textarea>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>


<div id="modal_form">
    <span id="modal_close">x</span>
    <div style="padding-left: 10px;display: inline-block;padding-top: 15px;font-family: Arial, Helvetica, sans-serif;">
        <a class="btn btn-lg btn-primary jpeg">Зберегти Jpeg</a>
    </div>
    <div style="padding-left: 10px;display: inline-block;padding-top: 15px;font-family: Arial, Helvetica, sans-serif;">
        <a class="btn btn-lg btn-primary gif">Зберегти Gif</a>
    </div>
</div>

<div id="modal_form_send">
    <span id="modal_close">x</span>


    <div style="height: 20px;">
        <span class="ok-message" style="display: none;font-family: Arial, Helvetica, sans-serif;">Повiдомлення досталено</span>
        <span class="error-message" style="display: none;font-family: Arial, Helvetica, sans-serif;">Невірний email</span>
    </div>

    <div style="margin: 10px 0px;">
        <input name="email" class="form-control" placeholder="Введiть email" style="font-family: Arial, Helvetica, sans-serif;">
    </div>

    <div style="margin: 10px 0px;">
        <div class="radio" style="font-family: Arial, Helvetica, sans-serif; display: inline-block;
    margin: 0 10px;">
            <label><input type="radio" name="image-type" value="png" checked>Jpeg</label>
        </div>
        <div class="radio" style="font-family: Arial, Helvetica, sans-serif;display: inline-block;
    margin: 0 10px;">
            <label><input type="radio" name="image-type" value="gif">Gif</label>
        </div>

    </div>

    <div style="font-family: Arial, Helvetica, sans-serif;">
        <a class="btn btn-lg btn-primary send-email">Вiдправити email</a>
    </div>
</div>
<div id="overlay"></div>


<a id="savingImages" style="display:none;" target="_blank">&nbsp;</a>
<audio src="melody-eng.wav" autoplay loop preload="auto" controls="controls"></audio> {{--autoplay--}}
<div id="mask-load">
    <img src="{{URL::asset('img/loading_apple.gif')}}" style="top: 25%;right: 25%;position: absolute;">
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->




<script type="text/javascript" src="{{URL::asset('js/jquery3.2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/colorpicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/eye.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/utils.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/layout.js?ver=1.0.2')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/blob.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/fileSaver.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/main3-blade.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/snow.js')}}"></script>
</body>
</html>
