<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Porsche gift card</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{URL::asset('css/colorpicker.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/layout.css')}}" />
    <link rel="stylesheet" media="screen" type="text/css" href="{{URL::asset('css/style.css')}}" />
</head>
<body>



<div class="container-fluid">
    <div class="container">
        {{--<div class="row">
            <div class="col-sm-12 logo">
                <img src="{{URL::asset('img/logo2-mini.jpg')}}">
            </div>
        </div>--}}
        <div class="row">
            &nbsp;
        </div>
        {{--<div class="row">
            <div class="col-sm-12">
                <h1 style="
    margin-bottom: 15px;
    margin-top: 15px;
">Создание Новогодней открытки</h1>
            </div>
        </div>--}}
        <div class="row">
            <div class="col-sm-12">
                    <img src="/{{ $table->url }}">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.11&appId=989845371031739';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-share-button"
                     data-href="http://1167864.pfgcasco.web.hosting-test.net/images/{{$table->id}}"
                     data-layout="button"
                     data-size="large"
                     data-mobile-iframe="true">
                    <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F1167864.pfgcasco.web.hosting-test.net%2F&amp;src=sdkpreparse">Поделиться</a>
                </div>

                <div class="row-form" style="display: inline-flex;">
                    <div>
                        <a class="btn btn-lg btn-primary save-image" href="/{{ $table->url }}" name="modal" download="gif-image">Зберегти</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--<div id="mask"><img src="{{URL::asset('img/loading_apple.gif')}}"></div>--}}
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->




<script type="text/javascript" src="{{URL::asset('js/jquery3.2.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/colorpicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/eye.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/utils.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/layout.js?ver=1.0.2')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/blob.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/fileSaver.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/main2.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/snow.js')}}"></script>
</body>
</html>
